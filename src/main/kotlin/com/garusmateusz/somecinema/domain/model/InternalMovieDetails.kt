package com.garusmateusz.somecinema.domain.model

import java.util.Date

data class InternalMovieDetails(
    val id: Long,
    val imdbId: String,
    val title: String,
    val price: Double,
    val rates: List<Rate>,
    val playDates: List<PlayDate>
)

data class BasicMovieDetails(
    val imdbId: String,
    val title: String,
    val price: Double,
    val rates: List<Rate>,
    val playDates: List<PlayDate>
)

data class Rate(
    val score: Int
)

data class PlayDate(
    val movieShowDate: Date
)

data class MovieDetailsWithRatings(
    val title: String,
    val description: String,
    val releaseDate: String,
    val ratings: List<MovieRating>,
    val runtime: String
)

data class MovieRating(val source: String, val value: String)