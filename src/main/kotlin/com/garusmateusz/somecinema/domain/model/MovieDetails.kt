package com.garusmateusz.somecinema.domain.model

sealed interface MovieDetails

data class FoundMovieDetails(
    val title: String,
    val year: String,
    val rated: String?,
    val released: String,
    val runtime: String,
    val genre: String?,
    val director: String?,
    val writer: String?,
    val actors: String?,
    val plot: String,
    val language: String?,
    val country: String?,
    val awards: String?,
    val poster: String?,
    val ratings: List<Rating>,
    val metaScore: String,
    val imdbRating: String,
    val imdbVotes: String,
    val imdbId: String,
    val type: String?,
    val dvd: String?,
    val boxOffice: String?,
    val production: String?,
    val website: String?,
    val response: String?
) : MovieDetails

data class ErrorMovieDetails(
    val response: String,
    val error: String
) : MovieDetails

data class Rating(
    val source: String,
    val value: String
)