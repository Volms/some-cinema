package com.garusmateusz.somecinema.domain.ports

import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import com.garusmateusz.somecinema.domain.model.BasicMovieDetails
import java.util.Date

interface IMovieRepository {
    fun movieExistsByImdbId(imdbId: String): Boolean
    fun addMovie(imdbId: String, price: Double, title: String): Long
    fun movieExistsById(id: Long): Boolean
    fun updatePriceOfMovie(id: Long, price: Double)
    fun addShowTimesForMovie(id: Long, showTimes: List<Date>)
    fun overrideShowTimesOfMovie(id: Long, showTimes: List<Date>)
    fun getInternalMovieDetails(id: Long): InternalMovieDetails
    fun getInternalMoviesDetails(): List<InternalMovieDetails>
    fun getBasicMovieDetails(imdbId: String): BasicMovieDetails?
    fun rateMovie(imdbId: String, rate: Int)
}