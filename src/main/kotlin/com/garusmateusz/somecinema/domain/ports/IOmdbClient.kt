package com.garusmateusz.somecinema.domain.ports

import com.garusmateusz.somecinema.domain.model.MovieDetails

interface IOmdbClient {
    fun getMovieDetails(apiKey: String, imdbId: String): MovieDetails
}