package com.garusmateusz.somecinema.domain

import com.garusmateusz.somecinema.domain.model.BasicMovieDetails
import com.garusmateusz.somecinema.domain.model.ErrorMovieDetails
import com.garusmateusz.somecinema.domain.model.FoundMovieDetails
import com.garusmateusz.somecinema.domain.model.MovieRating
import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import com.garusmateusz.somecinema.domain.model.MovieDetailsWithRatings
import com.garusmateusz.somecinema.domain.ports.IMovieRepository
import com.garusmateusz.somecinema.domain.ports.IOmdbClient
import org.springframework.stereotype.Service
import java.util.Date

@Service
class MovieService(
    private val movieRepository: IMovieRepository,
    private val omdbClient: IOmdbClient
) {
    fun movieExistsById(id: Long): Boolean {
        return movieRepository.movieExistsById(id)
    }

    fun movieExistsByImdbId(imdbId: String): Boolean {
        return movieRepository.movieExistsByImdbId(imdbId)
    }

    fun addMovie(apiKey: String, imdbId: String, price: Double): Long? {
        val movieDetails = omdbClient.getMovieDetails(apiKey, imdbId)
        if(movieDetails is ErrorMovieDetails) {
            return null
        }

        movieDetails as FoundMovieDetails
        return movieRepository.addMovie(movieDetails.imdbId, price, movieDetails.title)
    }

    fun updateMovieShowTimes(id: Long, shouldOverrideShowTimes: Boolean, showTimes: List<Date>) {
        when (shouldOverrideShowTimes) {
            true -> movieRepository.overrideShowTimesOfMovie(id, showTimes)
            false -> movieRepository.addShowTimesForMovie(id, showTimes)
        }
    }

    fun getInternalMoviesDetails(id: Long? = null): List<InternalMovieDetails> {
        return when (id) {
            null -> movieRepository.getInternalMoviesDetails()
            else -> listOf(movieRepository.getInternalMovieDetails(id))
        }
    }

    fun getBasicMovieDetails(imdbId: String): BasicMovieDetails? {
        return movieRepository.getBasicMovieDetails(imdbId)
    }

    fun getMovieDetailsWithRating(apiKey: String, imdbId: String): MovieDetailsWithRatings? {
        val omdbMovieDetails = omdbClient.getMovieDetails(apiKey, imdbId) as? FoundMovieDetails ?: return null
        val basicMovieDetails = movieRepository.getBasicMovieDetails(imdbId)

        val averageCinemaUsersScore = if (basicMovieDetails != null && basicMovieDetails.rates.isNotEmpty()) {
            basicMovieDetails.rates.sumOf { it.score }.toDouble() / basicMovieDetails.rates.size
        } else null

        val ratings = omdbMovieDetails.ratings.map { MovieRating(it.source, it.value) }.let {
            if(averageCinemaUsersScore != null) {
                it.plus(MovieRating("SomeCinema users", "${averageCinemaUsersScore}/5"))
            } else {
                it
            }
        }

        return MovieDetailsWithRatings(
            title = omdbMovieDetails.title,
            description = omdbMovieDetails.plot,
            releaseDate = omdbMovieDetails.released,
            ratings = ratings,
            runtime = omdbMovieDetails.runtime
        )
    }

    fun updateMoviePrice(id: Long, price: Double) {
        movieRepository.updatePriceOfMovie(id, price)
    }

    fun rateMovie(imdbId: String, rate: Int) {
        movieRepository.rateMovie(imdbId, rate)
    }
}