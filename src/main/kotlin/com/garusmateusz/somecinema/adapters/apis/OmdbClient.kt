package com.garusmateusz.somecinema.adapters.apis

import com.garusmateusz.somecinema.adapters.apis.lowlevel.LowLevelOmdbClient
import com.garusmateusz.somecinema.domain.model.ErrorMovieDetails
import com.garusmateusz.somecinema.domain.model.FoundMovieDetails
import com.garusmateusz.somecinema.domain.model.MovieDetails
import com.garusmateusz.somecinema.domain.model.Rating
import com.garusmateusz.somecinema.domain.ports.IOmdbClient
import org.springframework.stereotype.Component

@Component
class OmdbClient(private val lowLevelOmdbClient: LowLevelOmdbClient) : IOmdbClient {
    override fun getMovieDetails(apiKey: String, imdbId: String): MovieDetails {
        return lowLevelOmdbClient.getMovieDescription(apiKey, imdbId).let { movieResponse ->
            when (movieResponse.error) {
                null -> {
                    FoundMovieDetails(
                        title = movieResponse.title!!,
                        year = movieResponse.year!!,
                        rated = movieResponse.rated,
                        released = movieResponse.released!!,
                        runtime = movieResponse.runtime!!,
                        genre = movieResponse.genre,
                        director = movieResponse.director,
                        writer = movieResponse.writer,
                        actors = movieResponse.actors,
                        plot = movieResponse.plot!!,
                        language = movieResponse.language,
                        country = movieResponse.country,
                        awards = movieResponse.awards,
                        poster = movieResponse.poster,
                        ratings = movieResponse.ratings!!.map {
                            Rating(source = it.source, value = it.value)
                        },
                        metaScore = movieResponse.metascore!!,
                        imdbRating = movieResponse.imdbRating!!,
                        imdbVotes = movieResponse.imdbVotes!!,
                        imdbId = movieResponse.imdbId!!,
                        type = movieResponse.type,
                        dvd = movieResponse.dvd,
                        boxOffice = movieResponse.boxOffice,
                        production = movieResponse.production,
                        website = movieResponse.website,
                        response = movieResponse.response
                    )
                }
                else -> ErrorMovieDetails(movieResponse.response!!, movieResponse.error)
            }
        }
    }
}