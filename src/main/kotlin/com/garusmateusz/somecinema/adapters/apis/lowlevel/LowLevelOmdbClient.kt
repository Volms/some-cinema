package com.garusmateusz.somecinema.adapters.apis.lowlevel

import com.garusmateusz.somecinema.adapters.apis.lowlevel.models.OmdbMovieDetailsResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET

@FeignClient(
    name = "omdbapi",
    url = "\${omdbapi.url}"
)
interface LowLevelOmdbClient {
    @RequestMapping(
        method = [GET],
        value = ["/?apikey={apiKey}&i={imdbId}"],
        produces = ["application/json"]
    )
    fun getMovieDescription(
        @PathVariable("apiKey") apiKey: String,
        @PathVariable("imdbId") imdbId: String
    ) : OmdbMovieDetailsResponse
}