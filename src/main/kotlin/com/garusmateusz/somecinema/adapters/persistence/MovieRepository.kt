package com.garusmateusz.somecinema.adapters.persistence

import com.garusmateusz.somecinema.adapters.persistence.lowlevel.LowLevelMovieRepository
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.Movie
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.PlayDate
import com.garusmateusz.somecinema.domain.model.BasicMovieDetails
import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import com.garusmateusz.somecinema.domain.model.Rate
import com.garusmateusz.somecinema.domain.ports.IMovieRepository
import org.springframework.stereotype.Service
import java.util.Date
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.Rate as LowlevelModelsRate
import com.garusmateusz.somecinema.domain.model.PlayDate as DomainPlayDate

@Service
class MovieRepository(
    private val lowLevelMovieRepository: LowLevelMovieRepository
) : IMovieRepository {
    override fun movieExistsByImdbId(imdbId: String): Boolean {
        return lowLevelMovieRepository.existsByImdbId(imdbId)
    }

    override fun movieExistsById(id: Long): Boolean {
        return lowLevelMovieRepository.existsById(id)
    }

    override fun addMovie(imdbId: String, price: Double, title: String): Long {
        val movie = lowLevelMovieRepository.save(
            Movie(
                imdbId = imdbId,
                title = title,
                rates = mutableListOf(),
                price = price,
                playDates = mutableListOf()
            )
        )

        return movie.id
    }

    override fun addShowTimesForMovie(id: Long, showTimes: List<Date>) {
        val movie = lowLevelMovieRepository.findById(id).get()
        movie.playDates.addAll(showTimes.map { PlayDate(it) })
        movie.playDates = movie.playDates.distinctBy { it.movieShowDate }.sortedBy { it.movieShowDate }.toMutableList()
        lowLevelMovieRepository.save(movie)
    }

    override fun overrideShowTimesOfMovie(id: Long, showTimes: List<Date>) {
        val movie = lowLevelMovieRepository.findById(id).get()
        movie.playDates = showTimes.map { PlayDate(it) }.toMutableList()
        movie.playDates = movie.playDates.distinctBy { it.movieShowDate }.sortedBy { it.movieShowDate }.toMutableList()
        lowLevelMovieRepository.save(movie)
    }

    override fun updatePriceOfMovie(id: Long, price: Double) {
        val movie = lowLevelMovieRepository.findById(id).get()
        movie.price = price
        lowLevelMovieRepository.save(movie)
    }

    override fun getInternalMovieDetails(id: Long): InternalMovieDetails {
        val movie = lowLevelMovieRepository.findById(id).get()
        return InternalMovieDetails(
            id = movie.id,
            imdbId = movie.imdbId,
            title = movie.title,
            price = movie.price,
            rates = movie.rates.map { Rate(it.score) },
            playDates = movie.playDates.map { DomainPlayDate(it.movieShowDate) }
        )
    }

    override fun getInternalMoviesDetails(): List<InternalMovieDetails> {
        val movies = lowLevelMovieRepository.findAll().map { it }
        return movies.map { movie ->
            InternalMovieDetails(
                id = movie.id,
                imdbId = movie.imdbId,
                title = movie.title,
                price = movie.price,
                rates = movie.rates.map { Rate(it.score) },
                playDates = movie.playDates.map { DomainPlayDate(it.movieShowDate) })
        }
    }

    override fun getBasicMovieDetails(imdbId: String): BasicMovieDetails? {
        val movie = lowLevelMovieRepository.findByImdbId(imdbId).let {
            if (it.isPresent) {
                it.get()
            } else {
                return null
            }
        }

        return BasicMovieDetails(
            imdbId = movie.imdbId,
            title = movie.title,
            price = movie.price,
            rates = movie.rates.map { Rate(it.score) },
            playDates = movie.playDates.map { DomainPlayDate(it.movieShowDate) }
        )
    }

    override fun rateMovie(imdbId: String, rate: Int) {
        val movie = lowLevelMovieRepository.findByImdbId(imdbId).get()

        movie.rates.add(LowlevelModelsRate(rate))
        lowLevelMovieRepository.save(movie)
    }
}