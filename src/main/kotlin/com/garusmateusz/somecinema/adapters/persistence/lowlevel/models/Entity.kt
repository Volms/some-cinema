package com.garusmateusz.somecinema.adapters.persistence.lowlevel.models

import java.util.Date
import javax.persistence.CollectionTable
import javax.persistence.ElementCollection
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn

@Entity(name = "movie")
data class Movie(
    val imdbId: String,
    val title: String,
    @ElementCollection
    @CollectionTable(name = "rates", joinColumns = [JoinColumn(name = "id")])
    val rates: MutableList<Rate>,
    var price: Double,
    @ElementCollection
    @CollectionTable(name = "dates", joinColumns = [JoinColumn(name = "id")])
    var playDates: MutableList<PlayDate>
) {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0
}

@Embeddable
data class Rate(
    var score: Int
)

@Embeddable
data class PlayDate(
    var movieShowDate: Date
)