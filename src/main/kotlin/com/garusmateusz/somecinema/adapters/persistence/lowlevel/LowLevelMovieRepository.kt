package com.garusmateusz.somecinema.adapters.persistence.lowlevel

import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.Movie
import org.springframework.data.repository.CrudRepository
import java.util.Optional

interface LowLevelMovieRepository : CrudRepository<Movie, Long> {
    fun existsByImdbId(imdbId: String): Boolean
    fun findByImdbId(imdbId: String): Optional<Movie>
}