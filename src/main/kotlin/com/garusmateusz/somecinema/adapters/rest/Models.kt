package com.garusmateusz.somecinema.adapters.rest

import java.util.Date

data class UpdateMovieRequest(
    val showTimes: List<Date>? = null,
    val replaceOldShowTimes: Boolean = false,
    val price: Double? = null
)

data class InternalMovieDetailsResponse(
    val id: Long,
    val imdbId: String,
    val title: String,
    val price: Double,
    val rates: List<Rate>,
    val playDates: List<PlayDate>
)

data class Rate(
    val score: Int
)

data class PlayDate(
    val movieShowDate: Date
)

data class PublicMovieDetailsResponse(
    val title: String,
    val description: String,
    val releaseDate: String,
    val ratings: List<Rating>,
    val runtime: String
)

data class Rating(val source: String, val value: String)