package com.garusmateusz.somecinema.adapters.rest

import com.garusmateusz.somecinema.domain.MovieService
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("movies")
class MovieController(private val movieService: MovieService) {
    @RequestMapping("/show-times/{imdbId}", method = [RequestMethod.GET])
    fun getShowTimesOfMovie(@PathVariable imdbId: String): ResponseEntity<Any> {
        if (!movieService.movieExistsByImdbId(imdbId)) {
            return ResponseEntity.status(409).body("Movie with $imdbId doesn't exists.")
        }

        val moviePlayDates = movieService.getBasicMovieDetails(imdbId)?.playDates

        return ResponseEntity.ok().body(moviePlayDates)
    }

    @RequestMapping("/rate/{imdbId}/{rate}", method = [RequestMethod.PUT])
    fun rateMovie(@PathVariable imdbId: String, @PathVariable rate: Int): ResponseEntity<Any> {
        if (!movieService.movieExistsByImdbId(imdbId)) {
            return ResponseEntity.status(409).body("Movie with $imdbId doesn't exists.")
        }

        if(rate < 0 || rate > 5) {
            return ResponseEntity.badRequest().body("Rate should be from 0-5.")
        }

        movieService.rateMovie(imdbId, rate)

        return ResponseEntity.ok().build()
    }

    @RequestMapping("/details/{imdbId}", method = [RequestMethod.GET])
    fun getMovieDetails(@PathVariable imdbId: String, @RequestHeader headers: HttpHeaders): ResponseEntity<Any> {
        if (headers.getFirst(OMDB_API_HEADER_KEY) == null) {
            return ResponseEntity.status(409).body("Missing omdbapi header [OMDBHeaderKey].")
        }

        val response = movieService.getMovieDetailsWithRating(apiKey = headers.getFirst(OMDB_API_HEADER_KEY)!!, imdbId)

        return when (response) {
            null -> ResponseEntity.notFound().build()
            else -> ResponseEntity.ok().body(
                PublicMovieDetailsResponse(
                    title = response.title,
                    description = response.description,
                    releaseDate = response.releaseDate,
                    ratings = response.ratings.map { Rating(source = it.source, value = it.value) },
                    runtime = response.runtime
                )
            )
        }
    }
}