package com.garusmateusz.somecinema.adapters.rest

import com.garusmateusz.somecinema.domain.MovieService
import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@RequestMapping("admin/movies")
class AdminMovieController(private val movieService: MovieService) {
    @RequestMapping("/add/{imdbId}/price/{price}", method = [RequestMethod.PUT])
    fun addMovie(
        @PathVariable imdbId: String,
        @PathVariable price: Double,
        @RequestHeader headers: HttpHeaders
    ): ResponseEntity<Any> {
        if (headers.getFirst(OMDB_API_HEADER_KEY) == null) {
            return ResponseEntity.status(409).body("Missing omdbapi header [OMDBHeaderKey].")
        }

        if (movieService.movieExistsByImdbId(imdbId)) {
            return ResponseEntity.status(409).body("Movie with $imdbId already exists.")
        }

        val movieId = movieService.addMovie(
            apiKey = headers.getFirst(OMDB_API_HEADER_KEY)!!,
            imdbId = imdbId,
            price = price
        )

        return when(movieId) {
            null -> ResponseEntity.notFound().build()
            else -> ResponseEntity.created(URI.create("admin/movies/$movieId")).build()
        }
    }

    @RequestMapping("/update/{movieId}", method = [RequestMethod.POST])
    fun updateMovie(
        @PathVariable movieId: Long,
        @RequestBody updateMovieRequest: UpdateMovieRequest
    ): ResponseEntity<Any> {
        if (!movieService.movieExistsById(movieId)) {
            return ResponseEntity.status(409).body("Movie with $movieId doesn't exists.")
        }

        if (updateMovieRequest.showTimes != null) {
            movieService.updateMovieShowTimes(
                movieId,
                updateMovieRequest.replaceOldShowTimes,
                updateMovieRequest.showTimes
            )
        }

        if (updateMovieRequest.price != null) {
            movieService.updateMoviePrice(movieId, updateMovieRequest.price)
        }

        return ResponseEntity.ok().build()
    }

    @RequestMapping("/", method = [RequestMethod.GET])
    fun getMovies(): ResponseEntity<Any> {
        return ResponseEntity.ok().body(
            mapInternalMoviesDetailsToInternalMoviesDetailsResponse(movieService.getInternalMoviesDetails())
        )
    }

    @RequestMapping("/{movieId}", method = [RequestMethod.GET])
    fun getMovie(@PathVariable movieId: Long): ResponseEntity<Any> {
        if (!movieService.movieExistsById(movieId)) {
            return ResponseEntity.status(409).body("Movie with $movieId doesn't exists.")
        }

        return ResponseEntity.ok().body(
            mapInternalMoviesDetailsToInternalMoviesDetailsResponse(movieService.getInternalMoviesDetails(movieId))
        )
    }

    private fun mapInternalMoviesDetailsToInternalMoviesDetailsResponse(
        domainMovies: List<InternalMovieDetails>
    ): List<InternalMovieDetailsResponse> {
        return domainMovies.map {
            InternalMovieDetailsResponse(
                id = it.id,
                imdbId = it.imdbId,
                title = it.title,
                price = it.price,
                rates = it.rates.map { Rate(it.score) },
                playDates = it.playDates.map { PlayDate(it.movieShowDate) }
            )
        }
    }
}