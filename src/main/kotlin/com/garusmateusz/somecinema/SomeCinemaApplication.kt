package com.garusmateusz.somecinema

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
class SomeCinemaApplication

fun main(args: Array<String>) {
    runApplication<SomeCinemaApplication>(*args)
}
