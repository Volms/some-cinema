package com.garusmateusz.somecinema.domain

import com.garusmateusz.somecinema.adapters.apis.OmdbClient
import com.garusmateusz.somecinema.adapters.persistence.MovieRepository
import com.garusmateusz.somecinema.domain.model.BasicMovieDetails
import com.garusmateusz.somecinema.domain.model.ErrorMovieDetails
import com.garusmateusz.somecinema.domain.model.FoundMovieDetails
import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import com.garusmateusz.somecinema.domain.model.MovieDetailsWithRatings
import com.garusmateusz.somecinema.domain.model.MovieRating
import com.garusmateusz.somecinema.domain.model.Rate
import com.garusmateusz.somecinema.domain.model.Rating
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class MovieServiceTest {
    private val movieRepository: MovieRepository = mock()
    private val omdbClient: OmdbClient = mock()

    private val movieService = MovieService(movieRepository, omdbClient)

    companion object {
        private const val IMDB_ID = "ID"
        private const val TITLE = "TITLE"
        private const val API_KEY = "KEY"
        private const val PRICE = 1.0
        private const val MOVIE_ID = 1L
    }

    @Test
    fun movieAlreadyExists_itExists_true() {
        given(movieRepository.movieExistsByImdbId(IMDB_ID)).willReturn(true)

        val result = movieService.movieExistsByImdbId(IMDB_ID)

        assertThat(result).isTrue
    }

    @Test
    fun addMovie_foundMovieDetailsAreReturned_shouldAddMovieAndReturnMovieId() {
        val movieDetails = mock<FoundMovieDetails>()
        given(movieDetails.imdbId).willReturn(IMDB_ID)
        given(movieDetails.title).willReturn(TITLE)
        given(omdbClient.getMovieDetails(API_KEY, IMDB_ID)).willReturn(movieDetails)
        given(movieRepository.addMovie(IMDB_ID, PRICE, TITLE)).willReturn(MOVIE_ID)

        val result = movieService.addMovie(API_KEY, IMDB_ID, PRICE)

        assertThat(result).isEqualTo(MOVIE_ID)
    }

    @Test
    fun addMovie_errorMovieDetailsAreReturned_shouldNotAddMovieAndReturnNull() {
        val movieDetails = mock<ErrorMovieDetails>()
        given(omdbClient.getMovieDetails(API_KEY, IMDB_ID)).willReturn(movieDetails)

        val result = movieService.addMovie(API_KEY, IMDB_ID, PRICE)

        verifyNoMoreInteractions(movieRepository)
        assertThat(result).isEqualTo(null)
    }

    @Test
    fun updateMovieShowTimes_overrideShowTimes_shouldUseOverrideShowTimesOfMovie() {
        movieService.updateMovieShowTimes(MOVIE_ID, true, listOf())

        verify(movieRepository).overrideShowTimesOfMovie(MOVIE_ID, listOf())
    }

    @Test
    fun updateMovieShowTimes_doNotOverrideShowTimes_shouldUseAddShowTimesForMovie() {
        movieService.updateMovieShowTimes(MOVIE_ID, false, listOf())

        verify(movieRepository).addShowTimesForMovie(MOVIE_ID, listOf())
    }

    @Test
    fun getInternalMoviesDetails_idIdPassed_shouldReturnOneMovieDetail() {
        val movieDetails = mock<InternalMovieDetails>()
        given(movieRepository.getInternalMovieDetails(MOVIE_ID)).willReturn(movieDetails)

        val result = movieService.getInternalMoviesDetails(MOVIE_ID)

        assertThat(result).isEqualTo(listOf(movieDetails))
    }

    @Test
    fun getInternalMoviesDetails_idIdNotPassed_shouldReturnAllMoviesDetails() {
        val movieDetails = mock<InternalMovieDetails>()
        val secondMovieDetails = mock<InternalMovieDetails>()
        given(movieRepository.getInternalMoviesDetails()).willReturn(listOf(movieDetails, secondMovieDetails))

        val result = movieService.getInternalMoviesDetails()

        assertThat(result).isEqualTo(listOf(movieDetails, secondMovieDetails))
    }

    @Test
    fun getExternalMovieDetails_shouldReturnPublicMovieDetails() {
        val movieDetails = mock<BasicMovieDetails>()
        given(movieRepository.getBasicMovieDetails(IMDB_ID)).willReturn(movieDetails)

        val result = movieService.getBasicMovieDetails(IMDB_ID)

        assertThat(result).isEqualTo(movieDetails)
    }

    @Test
    fun getMovieDetailsWithRating_shouldReturnMovieDetailsWithMixedRatings() {
        val foundMovieDetails: FoundMovieDetails = mock()
        given(foundMovieDetails.title).willReturn(TITLE)
        given(foundMovieDetails.plot).willReturn("PLOT")
        given(foundMovieDetails.released).willReturn("RELEASED")
        given(foundMovieDetails.runtime).willReturn("RUNTIME")
        given(movieRepository.getBasicMovieDetails(IMDB_ID)).willReturn(
            BasicMovieDetails(
                imdbId = IMDB_ID,
                title = TITLE,
                price = PRICE,
                rates = listOf(Rate(4), Rate(1)),
                playDates = listOf()
            )
        )
        given(omdbClient.getMovieDetails(API_KEY, IMDB_ID)).willReturn(foundMovieDetails)
        given(foundMovieDetails.ratings).willReturn(listOf(Rating("Some wallpaper", "4/5")))

        val result = movieService.getMovieDetailsWithRating(API_KEY, IMDB_ID)

        assertThat(result).isEqualTo(
            MovieDetailsWithRatings(
                title = TITLE,
                description = "PLOT",
                releaseDate = "RELEASED",
                ratings = listOf(
                    MovieRating(source = "Some wallpaper", value = "4/5"),
                    MovieRating(source = "SomeCinema users", value = "2.5/5")
                ),
                runtime = "RUNTIME"
            )
        )
    }

    @Test
    fun getMovieDetailsWithRating_noFoundMovieDetailsReturned_shouldReturnNull() {
        val errorMovieDetails: ErrorMovieDetails = mock()
        given(omdbClient.getMovieDetails(API_KEY, IMDB_ID)).willReturn(errorMovieDetails)

        val result = movieService.getMovieDetailsWithRating(API_KEY, IMDB_ID)

        assertThat(result).isEqualTo(null)
    }

    @Test
    fun rateMovie_shouldRate() {
        movieService.rateMovie(IMDB_ID, 0)

        verify(movieRepository).rateMovie(IMDB_ID, 0)
    }
}