package com.garusmateusz.somecinema.adapters.persistence

import com.garusmateusz.somecinema.adapters.persistence.lowlevel.LowLevelMovieRepository
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.Movie
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.PlayDate
import com.garusmateusz.somecinema.adapters.persistence.lowlevel.models.Rate
import com.garusmateusz.somecinema.domain.model.InternalMovieDetails
import com.garusmateusz.somecinema.domain.model.BasicMovieDetails
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Optional

internal class MovieRepositoryTest {
    private val lowLevelMovieRepository: LowLevelMovieRepository = mock()

    private val movieRepository = MovieRepository(lowLevelMovieRepository)

    companion object {
        private const val IMDB_ID = "ID"
        private const val TITLE = "TITLE"
        private const val PRICE = 1.0
        private const val MOVIE_ID = 0L
    }

    @Test
    fun movieExistsByImdbId_itExists_true() {
        given(lowLevelMovieRepository.existsByImdbId(IMDB_ID)).willReturn(true)

        val result = movieRepository.movieExistsByImdbId(IMDB_ID)

        assertThat(result).isTrue
    }

    @Test
    fun movieExistsById_itExists_true() {
        given(lowLevelMovieRepository.existsById(MOVIE_ID)).willReturn(true)

        val result = movieRepository.movieExistsById(MOVIE_ID)

        assertThat(result).isTrue
    }

    @Test
    fun addMovie_returnsId() {
        val movie = mock<Movie>()
        given(
            lowLevelMovieRepository.save(
                Movie(
                    imdbId = IMDB_ID,
                    title = TITLE,
                    rates = mutableListOf(),
                    price = PRICE,
                    playDates = mutableListOf()
                )
            )
        ).willReturn(movie)
        given(movie.id).willReturn(MOVIE_ID)

        val result = movieRepository.addMovie(IMDB_ID, PRICE, TITLE)

        assertThat(result).isEqualTo(MOVIE_ID)
    }

    @Test
    fun addShowTimesForMovie_shouldAddModifiedMovieDatesAndSaveNonDuplicateInCorrectOrderToDatabase() {
        val moviePlayDates = listOf(createDate(2020, 10, 11, 11, 11))
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, moviePlayDates.map { PlayDate(it) }.toMutableList())
        given(lowLevelMovieRepository.findById(MOVIE_ID)).willReturn(Optional.of(movie))
        val newMoviePlayDates = listOf(
            createDate(2021, 11, 23, 17, 50),
            createDate(2021, 11, 22, 21, 37),
            createDate(2021, 11, 22, 21, 37),
            createDate(2021, 11, 22, 21, 37)
        )

        movieRepository.addShowTimesForMovie(MOVIE_ID, newMoviePlayDates)

        verify(lowLevelMovieRepository).save(
            movie.copy(
                playDates = listOf(
                    createDate(2020, 10, 11, 11, 11),
                    createDate(2021, 11, 22, 21, 37),
                    createDate(2021, 11, 23, 17, 50)
                ).map { PlayDate(it) }.toMutableList()
            )
        )
    }

    @Test
    fun overrideShowTimesOfMovie_shouldOverrideMoviePlayDatesAndSaveNonDuplicateInCorrectOrderToDatabase() {
        val moviePlayDates = listOf(createDate(2020, 10, 11, 11, 11))
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, moviePlayDates.map { PlayDate(it) }.toMutableList())
        given(lowLevelMovieRepository.findById(MOVIE_ID)).willReturn(Optional.of(movie))
        val newMoviePlayDates = listOf(
            createDate(2021, 11, 23, 17, 50),
            createDate(2021, 11, 23, 17, 50),
            createDate(2021, 11, 22, 21, 37)
        )

        movieRepository.overrideShowTimesOfMovie(MOVIE_ID, newMoviePlayDates)

        verify(lowLevelMovieRepository).save(
            movie.copy(
                playDates = listOf(
                    createDate(2021, 11, 22, 21, 37),
                    createDate(2021, 11, 23, 17, 50)
                ).map { PlayDate(it) }.toMutableList()
            )
        )
    }

    @Test
    fun updatePriceOfMovie_shouldUpdatePriceAndSaveNonDuplicateInCorrectOrderToDatabase() {
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, mutableListOf())
        given(lowLevelMovieRepository.findById(MOVIE_ID)).willReturn(Optional.of(movie))

        movieRepository.updatePriceOfMovie(MOVIE_ID, 66.6)

        verify(lowLevelMovieRepository).save(
            movie.copy(
                price = 66.6
            )
        )
    }

    @Test
    fun getInternalMovieDetails_shouldReturnDetails() {
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, mutableListOf())
        given(lowLevelMovieRepository.findById(MOVIE_ID)).willReturn(Optional.of(movie))

        val result = movieRepository.getInternalMovieDetails(MOVIE_ID)

        assertThat(result).isEqualTo(
            InternalMovieDetails(
                id = MOVIE_ID,
                imdbId = IMDB_ID,
                title = TITLE,
                price = 33.3,
                rates = listOf(),
                playDates = listOf()
            )
        )
    }

    @Test
    fun getExternalMovieDetails_shouldReturnDetails() {
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, mutableListOf())
        given(lowLevelMovieRepository.findByImdbId(IMDB_ID)).willReturn(Optional.of(movie))

        val result = movieRepository.getBasicMovieDetails(IMDB_ID)

        assertThat(result).isEqualTo(
            BasicMovieDetails(
                imdbId = IMDB_ID,
                title = TITLE,
                price = 33.3,
                rates = listOf(),
                playDates = listOf()
            )
        )
    }

    @Test
    fun rateMovie_shouldSaveMovieWithAddedRate() {
        val movie = Movie(IMDB_ID, TITLE, mutableListOf(), 33.3, mutableListOf())
        given(lowLevelMovieRepository.findByImdbId(IMDB_ID)).willReturn(Optional.of(movie))

        movieRepository.rateMovie(IMDB_ID, 5)

        verify(lowLevelMovieRepository).save(movie.copy(rates = mutableListOf(Rate(5))))
    }

    private fun createDate(year: Int, month: Int, day: Int, hour: Int, minute: Int): Date {
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return dateFormatter.parse("$year-$month-$day $hour:$minute:00")
    }
}