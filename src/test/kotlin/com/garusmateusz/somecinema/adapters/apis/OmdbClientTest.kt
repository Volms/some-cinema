package com.garusmateusz.somecinema.adapters.apis

import com.garusmateusz.somecinema.adapters.apis.lowlevel.LowLevelOmdbClient
import com.garusmateusz.somecinema.adapters.apis.lowlevel.models.OmdbMovieDetailsResponse
import com.garusmateusz.somecinema.domain.model.ErrorMovieDetails
import com.garusmateusz.somecinema.domain.model.FoundMovieDetails
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class OmdbClientTest {
    private val lowLevelOmdbClient: LowLevelOmdbClient = mock()

    private val omdbClient = OmdbClient(lowLevelOmdbClient)
    private val movieDetailsReponse = OmdbMovieDetailsResponse(
        title = "TITLE",
        year = "YEAR",
        released = "RELEASED",
        runtime = "RUNTIME",
        plot = "PLOT",
        ratings = listOf(),
        metascore = "META",
        imdbRating = "IMDB_RATING",
        imdbVotes = "IMDB_VOTES",
        imdbId = "IMDB_ID"
    )

    companion object {
        private const val API_KEY = "API_KEY"
        private const val IMDB_ID = "IMDB_ID"
    }

    @Test
    fun movieDetailsResponseHasError_shouldReturnErrorMovieDetails() {
        given(lowLevelOmdbClient.getMovieDescription(API_KEY, IMDB_ID)).willReturn(
            movieDetailsReponse.copy(error = "hasta la vista baby", response = "I will be back")
        )

        val result = omdbClient.getMovieDetails(API_KEY, IMDB_ID)

        assertThat(result).isInstanceOf(ErrorMovieDetails::class.java)
    }

    @Test
    fun movieDetailsResponseHasNoError_shouldReturnFoundMovieDetails() {
        given(lowLevelOmdbClient.getMovieDescription(API_KEY, IMDB_ID)).willReturn(movieDetailsReponse)

        val result = omdbClient.getMovieDetails(API_KEY, IMDB_ID)

        assertThat(result).isInstanceOf(FoundMovieDetails::class.java)
    }
}