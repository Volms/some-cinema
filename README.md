# Some Cinema application
## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Mandatory header](#Mandatory Header with token)
* [To Do](#to-do)

## General info
A simple spring-boot-application where administrator can add movie to his service, set a ticket price and show times.
The user can get this information, he can also rate a movie and see movie details. 

## Technologies
Project is created with:
* Spring [Web, Data-Jpa, Test]
* Feign
* H2Database which mimics PostgreSQL
* Mockito

## Setup
To run this project, go to project root localization

```
$ ./gradlew bootRun
```

To run tests, go to project root localization

```
$ ./gradlew test
```

Then call one of endpoints described un SomeCinemaApi.json

## Mandatory Header with token
Two endpoints:

    - /admin/movies/add/{imdbId}/price/{price}
    - /movies/details/{imdbId}

Require `OMDBHeaderKey` to be passed as a Header.

## To Do:

    - secure /admin endpoints with JWT token
    - add cache for OmdbAPI as we may exceed token usage limit
    - implement e2e tests
